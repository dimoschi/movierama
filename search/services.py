import json
import requests
import logging
from redis import Redis

from django.conf import settings
from django.http import HttpResponseServerError, HttpResponseBadRequest


class SearchService:

    # One hour TTl for search results
    CACHE_TTL = 60 * 60
    # One week TTL for results of details
    DETAILS_CACHE_TTL = 60 * 60 * 24 * 7

    def __init__(self):
        self.r = Redis(
            settings.REDIS_HOST,
            settings.REDIS_PORT
        )

    def get_movies(self, title=None, is_adult=False, page=1):
        page_results = self.get_page(title, is_adult, page)
        movie_details = self.get_movie_details(
            page_results['movie_ids'],
            page_results['movie_info']
        )

        view_results_dict = {
            'movie_details': movie_details,
            'total_results': page_results['total_results'],
            'total_pages': page_results['total_pages'],
        }

        return view_results_dict

    def get_page(self, title=None, is_adult=False, page=1):
        """
        Search for the movies using the title. There is an option to search
        with adult movies included. Returns only the results for a single page,
        (TMDb returns only first page if None requested).

        title: The movie title. None will return the now playing movies
        is_adult (True/False): Include adult movies
        page: Page to retrieve results

        """
        cached_result = self.r.get('{}'.format(title))
        if cached_result is not None:
            try:
                cached_result = json.loads(cached_result.decode("utf-8"))
            except AttributeError as err:
                logger.warning(msg=err)
                pass
            else:
                return cached_result
        params = {'page': page}
        # If the user didn't provide any title we return the Now Playing
        if title == '':
            url = settings.MOVIEDB_URLS['now_playing']
        else:
            url = settings.MOVIEDB_URLS['movies']
            params.update({
                'query': title,
                'include_adult': 'true' if is_adult else 'false'
            })
        page_results = self._do_request(url, params=params)
        movie_ids, movie_info = self._format_results(page_results)

        page_results_dict = {
            'total_results': page_results['total_results'],
            'total_pages': page_results['total_pages'],
            'movie_ids': movie_ids,
            'movie_info': movie_info
        }

        serialized_result = json.dumps(page_results_dict)

        # Cache the search results for 1 hour
        if title != '':
            self.r.setex(
                '{}'.format(title),
                serialized_result,
                self.CACHE_TTL
            )

        return page_results_dict

    def get_movie_details(self, movie_ids, extra_info=None):
        """
        Searche for the details required for each movie.

        movies_ids: The ids of the movies returned from the initial query
        """
        result_dict = {}

        for movie_id in movie_ids:
            cached_result = self.r.get('movierama:{}'.format(movie_id))
            if cached_result is not None:
                try:
                    cached_result = json.loads(cached_result.decode("utf-8"))
                except AttributeError as err:
                    logger.warning(msg=err)
                    pass
                result_dict.update(cached_result)
                continue

            url = '{}/{}'.format(
                settings.MOVIEDB_URLS['details'], str(movie_id)
            )

            params = {
                'append_to_response': 'reviews,credits'
            }

            new_result = self._do_request(url, params=params)

            result_dict[str(movie_id)] = {
                'movie_title': new_result['title'],
                'movie_overview': new_result['overview'],
                'year_of_production': new_result['release_date'][:4],
                'starring_actors': [],
                'total_reviews': new_result['reviews']['total_results'],
                'poster_url': '{}{}'.format(
                    settings.MOVIE_POSTER_92_BASE,
                    extra_info[str(movie_id)]['poster_path']
                ) if extra_info is not None else None
            }

            actors_list = new_result['credits']['cast']
            for actor in actors_list:
                result_dict[str(movie_id)]['starring_actors'].append({
                    'id': actor['id'],
                    'name': actor['name'],
                    'role': actor['character']
                })

            serialized_result = json.dumps(result_dict[str(movie_id)])

            self.r.setex(
                'movierama:{}'.format(movie_id),
                serialized_result,
                self.DETAILS_CACHE_TTL
            )

        return result_dict

    @staticmethod
    def _do_request(url, params=None):
        params = params if params is not None else {}

        try:
            api_key = getattr(settings, 'API_KEY')
        except AttributeError as err:
            logger.exception(msg=err)
            raise HttpResponseBadRequest()
        else:
            params.update({
                'api_key': api_key,
            })

        try:
            response = requests.request("GET", url, params=params)
            response.raise_for_status()
            logger.info(response.url)
        except requests.HTTPError as err:
            logger.warning(msg=err)
            raise HttpResponseServerError()

        try:
            json_response = response.json()
        except json.JSONDecodeError as err:
            logger.warning(msg=err)
            raise HttpResponseServerError()
        return json_response

    def _format_results(self, results):
        # Format the results for easier manipulation
        id_list = []
        info = {}

        for r in results['results']:
            id_list.append(r.get('id'))
            info[str(r.get('id'))] = r
        return id_list, info


# Get an instance of a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
)

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)
