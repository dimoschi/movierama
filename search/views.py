import urllib
from django import views
from django.shortcuts import render
from django.core.urlresolvers import reverse

from search import forms, services


class SearchMovieView(views.View):
    template = "search/get_movie.html"
    form = forms.SearchForm

    def get(self, request):
        """
        Docstring
        """
        search_service = services.SearchService()
        form = self.form(self.request.GET)

        if not form.is_valid():
            # raise BadRequest
            pass

        results = search_service.get_movies(
            title=form.cleaned_data['search_term'],
            is_adult=form.cleaned_data['is_adult'],
            page=form.cleaned_data['page']
        )

        prev_url, next_url = self.get_page_direction(
            form.cleaned_data, results['total_pages']
        )

        return render(request, self.template, context={
            'form': self.form(form.cleaned_data),
            'results': results['movie_details'],
            'total_results': results['total_results'],
            'total_pages': results['total_pages'],
            'next_url': next_url,
            'prev_url': prev_url,
        })

    def get_page_direction(self, params, total_pages):
        """
        We use the query results to create a simple navigation
        """
        url_params = dict(params)
        page = url_params['page']
        base = reverse('get_movie')
        next_url = None
        prev_url = None

        if not params['page'] >= total_pages:
            url_params['page'] = page + 1
            next_url = '{}?{}'.format(
                base,
                urllib.parse.urlencode(url_params)
            )
        if not params['page'] == 1:
            url_params['page'] = page - 1
            prev_url = '{}?{}'.format(
                base,
                urllib.parse.urlencode(url_params)
            )
        return prev_url, next_url
