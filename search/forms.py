from django import forms


class SearchForm(forms.Form):
    search_term = forms.CharField(
        max_length=100,
        required=False,
    )
    is_adult = forms.BooleanField(
        label='Search for adult movies?',
        required=False,
        initial=False,
    )
    page = forms.IntegerField(
        required=False
    )

    def clean_page(self):
        page = self.cleaned_data['page']
        if not isinstance(page, int):
            return 1
        if page >= 1000:
            page = 1000
        elif page < 1:
            page = 1
        return page
