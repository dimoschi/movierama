Some info about the repository

This is a project for personal use to retrieve information from TMDb and is built with __Python 3.5.2__ and __Django 1.10.4__

How do I get set up?

* Mac OS/X

1. First make sure you have Python 3.5.2 by running `python3 -V` and find where is installed `which python3`.

2. Also you need Redis installed for caching the results. You can easily install it with Brew `brew install redis`.

3. Create a virtual environment & activate it (optional). Assuming that you have virtualenvwrapper installed you can run: `mkvirtualenv --python=<path_from_which_python3> <venv_name>` (e.g. `mkvirtualenv --python=/usr/local/bin/python3 movierama`).

Then run `workon <venv_name>` to activate the virtual environment.

4. Clone the repository `git clone https://dimoschi@bitbucket.org/dimoschi/movierama.git`

5. Go to movierama folder and run `pip install -r requirements/base.txt`

6. If everything went well you can run `./manage.py runserver` and access movierama in your browser at `127.0.0.1:8000`

* Windows

N/A

* Linux

N/A

Author: Dimosthenis Schizas (dimoschi@gmail.com)
