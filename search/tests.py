import urllib

from unittest.mock import patch, MagicMock

from django.test import TestCase, Client
from django.conf import settings
from django.core.urlresolvers import reverse

from search import services, forms
from search.views import SearchMovieView


class SearchServiceTests(TestCase):

    @patch('search.services.SearchService.get_page')
    @patch('search.services.SearchService.get_movie_details')
    def test_get_movies(self, mock_get_movie_details, mock_get_page):
        assert services.SearchService.get_page is mock_get_page
        assert services.SearchService.get_movie_details is mock_get_movie_details
        mock_get_page.return_value = {
            'total_results': 1,
            'total_pages': 1,
            'movie_ids': [1],
            'movie_info': {'1': {'id': 1, 'title': 'Star Wars'}}
        }
        mock_get_movie_details.return_value = {
            '1': {
                'movie_title': 'Star Wars',
                'movie_overview': 'The best movie ever made',
                'year_of_production': '9999',
                'starring_actors': [{
                    'id': 1,
                    'name': 'Peter Mayhew',
                    'role': 'Chewie'
                }],
                'total_reviews': '9000',
                'poster_url': None
            }
        }

        expected_results = {
            'movie_details': {
                '1': {
                    'movie_title': 'Star Wars',
                    'movie_overview': 'The best movie ever made',
                    'year_of_production': '9999',
                    'starring_actors': [{
                        'id': 1,
                        'name': 'Peter Mayhew',
                        'role': 'Chewie'
                    }],
                    'total_reviews': '9000',
                    'poster_url': None
                }
            },
            'total_pages': 1,
            'total_results': 1
        }
        actual_results = services.SearchService().get_movies()
        self.assertEqual(actual_results, expected_results)

    @patch.object(services.SearchService, '_do_request')
    def test_get_movie_details(self, mock_do_request):
        """Test get_movie method without caching"""
        # Mock _do_request
        mock_do_request.return_value = {
            'title': 'Star Wars',
            'overview': 'The best movie ever made',
            'release_date': '9999-13-33',
            'reviews': {
                'total_results': '9000'
            },
            'credits': {
                'cast': [{
                    'id': 1,
                    'name': 'Peter Mayhew',
                    'character': 'Chewie'
                }]
            }
        }
        expected_results = {
            '1': {
                'movie_title': 'Star Wars',
                'movie_overview': 'The best movie ever made',
                'year_of_production': '9999',
                'starring_actors': [{
                    'id': 1,
                    'name': 'Peter Mayhew',
                    'role': 'Chewie'
                }],
                'total_reviews': '9000',
                'poster_url': None
            }
        }
        actual_results = services.SearchService().get_movie_details([1])
        # Assert the actual_results from cache are the expected
        self.assertEqual(actual_results, expected_results)

        with patch('search.services.Redis.get') as redis_get_mock:
            redis_get_mock.return_value = None
            actual_results = services.SearchService().get_movie_details([1])
            # Assert the actual_results from request are the expected
            self.assertEqual(actual_results, expected_results)

    @patch.object(services.SearchService, '_do_request')
    def test_get_page(self, mock_do_request):
        """Test get_page method without caching"""
        # Mock request to avoid hitting the TMDb API
        mock_do_request.return_value = {
            'total_results': 1,
            'total_pages': 1,
            'results': [{
                'title': 'Star Wars',
                'id': 1
            }]
        }
        # Results we expect to receive from the get_page method
        expected_results = {
            'total_results': 1,
            'total_pages': 1,
            'movie_ids': [1],
            'movie_info': {'1': {'id': 1, 'title': 'Star Wars'}}
        }

        actual_results = services.SearchService().get_page()
        # Assert the actual_results from cache are the expected
        self.assertEqual(actual_results, expected_results)

        # Call the get_page providing an empty title to return now_playing
        actual_results = services.SearchService().get_page(title='')
        # Assert the actual_results from cache are the expected
        self.assertEqual(actual_results, expected_results)

        # Mock Redis response to avoid pulling from cache
        with patch('search.services.Redis.get') as redis_get_mock:
            redis_get_mock.return_value = None
            actual_results = services.SearchService().get_page()
            # Assert the actual_results from request are the expected
            self.assertEqual(actual_results, expected_results)

    def test_format_results(self):
        """Test _format_results method"""
        results = {
            'results': [{
                'id': 1,
                'movie_title': 'The Big Lebowski',
                'movie_overview': 'The best movie ever made',
                },
                {
                'id': 2,
                'movie_title': 'The Big',
                'movie_overview': 'A movie about big'
            }]
        }
        expected_ids = [1, 2]
        expected_info = {
            '1': {
                'id': 1,
                'movie_title': 'The Big Lebowski',
                'movie_overview': 'The best movie ever made',
            },
            '2': {
                'id': 2,
                'movie_title': 'The Big',
                'movie_overview': 'A movie about big'
            }
        }
        actual_ids, actual_info = services.SearchService._format_results(
            self, results
        )
        self.assertEqual(actual_ids, expected_ids)
        self.assertEqual(actual_info, expected_info)

    @patch('search.services.requests.request')
    def test_do_request(self, req):
        """Test _do_request method"""
        url = MagicMock()
        api = settings.API_KEY
        services.SearchService._do_request(url)
        req.assert_called_once_with(
            'GET', url, params={'api_key': api}
        )


class SearchMovieViewTests(TestCase):

    def test_valid_data(self):
        data = {
            'search_term': "Star Wars",
            'is_adult': False,
        }
        form = forms.SearchForm(data)
        self.assertTrue(form.is_valid())

    @patch('search.views.services.SearchService.get_movies')
    def test_view_response(self, mock_get_movies):
        mock_get_movies.return_value = {
            'total_results': 1,
            'total_pages': 1,
            'movie_details': [{
                'title': 'Star Wars',
                'year_of_production': '9999-13-13'
            }]
        }

        c = Client()
        response = c.get('/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.resolver_match.func.__name__,
            SearchMovieView.as_view().__name__
        )

    def test_post_view(self):
        """Test that post is not accepted in SearchMovieView."""
        c = Client()
        response = c.post('/')
        # We use status_code 405, which is Method Not Allowed.
        self.assertEqual(response.status_code, 405)

    def test_page_direction(self):
        data = {
            'search_term': "Star Wars",
            'is_adult': False
        }
        form = forms.SearchForm(data)
        form.is_valid()

        total_pages = 1
        previous, next = SearchMovieView().get_page_direction(
            form.cleaned_data, total_pages
        )
        self.assertEqual(previous, None)
        self.assertEqual(next, None)

        total_pages = 2
        previous, next = SearchMovieView().get_page_direction(
            form.cleaned_data, total_pages
        )
        data.update({
            'page': 2
        })
        expected_next = '{}?{}'.format(
            reverse('get_movie'),
            urllib.parse.urlencode(data))
        self.assertEqual(previous, None)
        self.assertEqual(next, expected_next)

        form = forms.SearchForm(data)
        form.is_valid()
        previous, next = SearchMovieView().get_page_direction(
            form.cleaned_data, total_pages
        )
        data.update({
            'page': 1
        })
        expected_previous = '{}?{}'.format(
            reverse('get_movie'),
            urllib.parse.urlencode(data))
        self.assertEqual(previous, expected_previous)
        self.assertEqual(next, None)
