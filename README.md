# README #
[ ![Codeship Status for dimoschi/movierama](https://app.codeship.com/projects/c733c310-9f52-0134-2beb-32346381ad9a/status?branch=master)](https://app.codeship.com/projects/189249)

### Some info about the repository ###

* This is a project for personal use to retrieve information from TMDb
* The project is built with __Python 3.5.2__ and __Django 1.10.4__

### How do I get set up? ###

## Mac OS/X ##

1. First make sure you have Python 3.5.2 by running `python3 -V` and find where is installed `which python3`.
2. Also you need Redis installed for caching the results. You can easily install it with Brew `brew install redis`.
3. Create a virtual environment & activate it (optional). Assuming that you have virtualenvwrapper installed you can run: `mkvirtualenv --python=<path_from_which_python3> <venv_name>` (e.g. `mkvirtualenv --python=/usr/local/bin/python3 movierama`).
Then run `workon <venv_name>` to activate the virtual environment.
4. Clone the repository `git clone https://dimoschi@bitbucket.org/dimoschi/movierama.git`
5. Go to movierama folder and run `pip install -r requirements/base.txt`.
6. You can run optionally `python manage.py migrate`.
7. If everything went well you can run `./manage.py runserver` and access movierama in your browser at `127.0.0.1:8000`.

## Windows ##

__N/A__

## Linux ##

__N/A__

Authored by Dimosthenis Schizas [dimoschi@gmail.com](dimoschi@gmail.com)
