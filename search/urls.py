from django.conf.urls import url
from search import views

urlpatterns = [
    url(r'^$', views.SearchMovieView.as_view(), name='get_movie'),
]
